const Sequelize = require('sequelize')

const { sequelize } = require('../src/config/db')

const User = sequelize.define('Users', {
    googleID: Sequelize.STRING,
    displayName: Sequelize.STRING,
    email: Sequelize.STRING
})

module.exports = { User }
