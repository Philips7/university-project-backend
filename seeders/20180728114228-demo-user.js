'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('Users', [{
            createdAt: new Date(),
            updatedAt: new Date(),
            displayName: 'Dominik Filip',
            email: 'test@test.com',
            googleID: '1234567890',
        }], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Users', null, {});
    }
};
