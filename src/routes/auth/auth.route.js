const passport = require('passport')
const router = require('express').Router()

// auth login
router.get('/login', (req, res) => {
    res.send('LOGIN')
})

// auth logout
router.get('/logout', (req, res) => {
    res.send('Logging out')
})

// auth with google
router.get('/google', passport.authenticate("google", {
    scope: ['profile']
}))

//callback route for google to redirect

router.get('/google/redirect', passport.authenticate('google'), (req, res) => {
    res.send(req.user)
})

module.exports = router
