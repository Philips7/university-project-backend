const passport = require('passport')
const GoogleStrategy = require('passport-google-oauth20')

const {User} = require('../../models/user')

passport.serializeUser((user, done) => {
    done(null, user.googleID);
});

passport.deserializeUser((user, done) => {
    User.findOne({
        where: {
            googleID: user.googleID
        }
    }).then(() => {
        done(null, user)
    })
});

passport.use(
    new GoogleStrategy({
        clientID: "667981059740-aaa1boceq8eqpftlspcf42cm70la3mnv.apps.googleusercontent.com",
        clientSecret: "nqOeuweg9rqX8MwoiHqShCay",
        callbackURL: '/auth/google/redirect',
    }, async (accessToken, refreshToken, profile, done) => {
        const user = await User.findOne({
            where: {
                googleID: profile.id
            }
        })
        if (user) {
            done(null, user.dataValues)
        } else {
            const user = User.build({
                displayName: profile.displayName,
                email: 'test@test2',
                googleID: profile.id
            })
            user.save()
                .then(user => {
                    done(null, user.dataValues)
                })
        }
    })
)
