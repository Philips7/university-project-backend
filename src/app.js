const express = require('express')
const passport = require('passport')
const cookieSession = require('cookie-session')

const app = express()
app.use(cookieSession({
    maxAge: 24 * 60 * 60 * 1000,
    keys: ['cookieSecret']
}))

app.use(passport.initialize());
app.use(passport.session());

const passportSetup = require('./config/passport.setup')
const { sequelize } = require('./config/db')
const authRoutes = require('./routes/auth/auth.route')

// set up view engine
app.set('view engine', 'ejs')

app.use('/auth', authRoutes)

// create home route

app.get('/', (req, res) => {
    res.render('home');
})

app.listen(8080, () => {
    console.log('Listening on port 8080')
})
